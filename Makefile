# Makefile for boot-66serv

VERSION = $$(git describe --tags| sed 's/-.*//g;s/^v//;')
PKGNAME = boot-66serv

EXECLINE_SHEBANGPREFIX = /usr/bin
BINDIR = /usr/bin


DATA = `find data/boot/scripts -type f`
CONF = `find conf/ -type f`

DIRS= \
	/usr/bin \
	/etc/66 \
	/etc/66/data/boot/scripts \
	/etc/66/conf/ \
	/etc/66/service/boot \
	/usr/share/licenses

install:
	install -dm755 $(addprefix $(DESTDIR),$(DIRS))
	
	for i in init stage2 stage2.tini stage3 poweroff reboot $(DATA) ; do \
		sed -i 's,@EXECLINE_SHEBANGPREFIX@,$(EXECLINE_SHEBANGPREFIX),' $$i; \
	done 
	
	for i in $(DATA) shutdown init ; do \
		sed -i 's,@BINDIR@,$(BINDIR),' $$i; \
	done
		
	for i in $(CONF); do \
		install -m644 $$i $(DESTDIR)/etc/66/$$i; \
	done
	
	for i in $(DATA); do \
		install -m755 $$i $(DESTDIR)/etc/66/$$i; \
	done
	
	ln -sf /etc/66/data/boot/scripts/66.local $(DESTDIR)/etc/66.local
	
	cp -P -a service/boot $(DESTDIR)/etc/66/service/
		
	install -m755 init $(DESTDIR)$(BINDIR)
	for i in stage2 stage2.tini stage3 reboot shutdown poweroff; do \
		install -m755 $$i $(DESTDIR)/etc/66/$$i;\
	done
	ln -sf /etc/66/poweroff $(DESTDIR)$(BINDIR)
	ln -sf /etc/66/reboot $(DESTDIR)$(BINDIR)
	ln -sf /etc/66/shutdown $(DESTDIR)$(BINDIR)
	
	install -m644 66.conf $(DESTDIR)/etc/66
	ln -sf /etc/66/66.conf $(DESTDIR)/etc/66.conf
	
	install -Dm644 LICENSE $(DESTDIR)/usr/share/licenses/$(PKGNAME)/LICENSE
	
version:
	@echo $(VERSION)
	
.PHONY: install version
